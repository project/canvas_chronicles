<?php

use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_HOOK() for menu.html.twig.
 */


function gallery_preprocess_page(&$variables)
{
  // Get subheading.
  $subheading = theme_get_setting('subheading');
  if (!empty($subheading)) {
    $variables['subheading'] = $subheading;
  } else {
    $variables['subheading'] = 'A personal Blog';
  }
  $mainheading = theme_get_setting('mainheading');
  if (!empty($mainheading)) {
    $variables['mainheading'] = $mainheading;
  } else {
    $variables['mainheading'] = 'Chronicles';
  }
  $description = theme_get_setting('description');
  if (!empty($description)) {
    $variables['description'] = $description;
  } else {
    $variables['description'] = 'Most popular Curious Chronicles theme for Contribution';
  }
  $blogheading = theme_get_setting('blogheading');
  if (!empty($blogheading)) {
    $variables['blogheading'] = $blogheading;
  } else {
    $variables['blogheading'] = 'Blog Heading';
  }

  $button_link = theme_get_setting('button_link');
  if (!empty($button_link)) {
    $variables['button_link'] = $button_link;
  }

  $button_text = theme_get_setting('button_text');
  if (!empty($button_text)) {
    $variables['button_text'] = $button_text;
  }

  $fid = theme_get_setting('image');
  $variables['image'] = 'https://pltfrmsearch.com/wp-content/uploads/2022/12/banner.jpg';
  if (!empty($fid)) {
    $fid = theme_get_setting('image');
    if (!empty($fid)) {
      $file = File::load($fid[0]);
      if ($file) {
        $file_uri = $file->getFileUri();
        $file_path = \Drupal::service('file_url_generator')->generateString($file_uri);
        $variables['image'] = $file_path;
      }
    }
  } 

}

function gallery_preprocess_region(&$variables)
{
  // Get Facebook Icon.
  $facebook = theme_get_setting('facebook');
  if (!empty($facebook)) {
    $variables['facebook'] = $facebook;
  }

    // Get Twitter Icon.
    $twitter = theme_get_setting('twitter');
    if (!empty($twitter)) {
      $variables['twitter'] = $twitter;
    }

    // Get Instagram Icon.
    $instagram = theme_get_setting('instagram');
    if (!empty($instagram)) {
      $variables['instagram'] = $instagram;
    }

    // Get Linkedin Icon.
    $linkedin = theme_get_setting('linkedin');
    if (!empty($linkedin)) {
      $variables['linkedin'] = $linkedin;
    }


    // Get Search Field.
    $footer_search = theme_get_setting('footer_search');
    if (!empty($footer_search)) {
      $variables['footer_search'] = $footer_search;
    }
}


/**
 * @file
 * Theme settings in this file.
 */
/**
 * Implements hook_form_system_theme_settings_alter().
 */

function gallery_form_system_theme_settings_alter(&$form, $form_state)
{

  $form['landing_page'] = [
    '#type' => 'details',
    '#title' => t('Landing Page'),
    '#open' => TRUE,
    '#description' => t('This section contains settings related to the landing page.'),
    '#attributes' => ['class' => ['custom-class']], // Add your custom class here
  ];

  // <------------------------- HERO SECTION START --------------------------------->


  $form['landing_page']['mainbanner'] = [
    '#type' => 'details',
    '#title' => t('Hero Section'),
  ];

  $form['landing_page']['mainbanner']['image'] = [
    '#type' => 'managed_file',
    '#title' => t('Banner'),
    '#upload_location' => 'public://',
    '#default_value' => theme_get_setting('image'),
    '#attributes' => ['class' => ['file-import-input']],
  ];

  $form['landing_page']['mainbanner']['mainheading'] = [
    '#type' => 'textfield',
    '#title' => t('Main Heading'),
    '#default_value' => theme_get_setting('mainheading'),
  ];


  $form['landing_page']['mainbanner']['description'] = [
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => theme_get_setting('description'),
  ];

  $form['landing_page']['mainbanner']['button'] = [
    '#type' => 'details',
    '#title' => t('Button'),
    '#open' => TRUE, // Set the details element to be open by default
    '#attributes' => ['class' => ['custom-details-class']], // Add a custom CSS class
  ];

  $form['landing_page']['mainbanner']['button']['button_link'] = [
    '#type' => 'textfield',
    '#title' => t('Button Link'),
    '#default_value' => theme_get_setting('button_link'),
    '#description' => t('Enter the URL where the button should redirect.'),
  ];

  $form['landing_page']['mainbanner']['button']['button_text'] = [
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => theme_get_setting('button_text'),
  ];


  // <----------------------------------- HERO SECTION END ---------------------------------> 



  // FOOTER ICONS BLOCK

  $form['landing_page']['footer_icons'] = [
    '#type' => 'details',
    '#title' => t('Footer Social Links'),
  ];

  $form['landing_page']['footer_icons']['facebook'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#default_value' => theme_get_setting('facebook'),
  ];


  $form['landing_page']['footer_icons']['twitter'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#default_value' => theme_get_setting('twitter'),
  ];

  $form['landing_page']['footer_icons']['instagram'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#default_value' => theme_get_setting('instagram'),
  ];

  $form['landing_page']['footer_icons']['linkedin'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin'),
    '#default_value' => theme_get_setting('linkedin'),
  ];

  $form['landing_page']['footer_icons']['footer_search'] = [
    '#type' => 'checkbox',
    '#title' => t('Want to Explore More!'),
    '#default_value' => theme_get_setting('footer_search'),
  ];
}
