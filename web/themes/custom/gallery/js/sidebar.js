const galleryHeader = ($) => {
  $(document).ready(function () {
    $(window).on("scroll", function () {
      if ($(window).scrollTop() > 100) {
        $(".gallery__header").addClass("header__active");
      } else {
        //remove the background property so it comes transparent again (defined in your css)
        $(".gallery__header").removeClass("header__active");
      }
    });
  });
};
galleryHeader(jQuery);